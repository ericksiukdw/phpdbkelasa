<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "contohdb";

$conn = new mysqli($servername,$username,$password,$dbname);

if($conn->connect_error){
    die("Connection failed");
}

$sql = "select * from Kategori order by NamaKategori asc";
$result = $conn->query($sql);

echo "<table border=1>";
echo "<tr><th>Kategori ID</th><th>Nama Kategori</th></tr>";
//jika ada record/ ditemukan record di table
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
        echo "<tr>";
        echo "<td>".$row["KategoriID"]."</td>"."<td>".$row["NamaKategori"]."</td>";
        echo "</tr>";
    }
}else {
    echo "Tidak ada data di table Kategori";
}
echo "</table>";

$conn->close();

?>